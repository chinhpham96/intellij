package sample;

import com.sound.PlayAudio;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    private Button btnRecord;

    @FXML
    private Button btnPlay;

    @FXML
    private RadioButton radioRecord;

    @FXML
    private RadioButton radioFile;

    @FXML
    private TextField urlFile;

    public void recordAction(ActionEvent actionEvent) {
        if (btnRecord.getText().equals("Record")) {
            btnRecord.setText("Stop");
            btnPlay.setDisable(true);
        } else {
            btnRecord.setText("Record");
            btnPlay.setDisable(false);
        }

    }

    public void playAction(ActionEvent actionEvent) {
        String pathFile = urlFile.getText().trim();
        System.out.println(pathFile);
        PlayAudio.playAudio(pathFile);
    }

    public void radioRecordAction(ActionEvent actionEvent) {
        btnRecord.setDisable(false);
        btnPlay.setDisable(true);
    }

    public void radioFileAction(ActionEvent actionEvent) {
        btnRecord.setDisable(true);
        btnPlay.setDisable(false);
    }

    public void urlFileAction(ActionEvent actionEvent) {
    }
}
