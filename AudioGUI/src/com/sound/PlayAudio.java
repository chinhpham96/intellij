package com.sound;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class PlayAudio {
    private static final int EXTERNAL_BUFFER_SIZE = 128000;

    private static void printUsageAndExit() {
        out("SimpleAudioPlayer: usage:");
        out("\tjava SimpleAudioPlayer <soundfile>");
        System.exit(1);
    }


    private static void out(String strMessage) {
        System.out.println(strMessage);
    }

    public static void playAudio(String args) {
        String strFilename = args;
        System.out.println(args);
        File soundFile = new File(strFilename);

        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(soundFile);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        AudioFormat audioFormat = audioInputStream.getFormat();
        System.out.println(audioFormat);

        SourceDataLine line = null;
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
        try {

            line = (SourceDataLine) AudioSystem.getLine(info);
            line.open(audioFormat);

        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }

        line.start();

        byte[] bytesBuffer = new byte[EXTERNAL_BUFFER_SIZE];
        int bytesRead = -1;

        try {
            while ((bytesRead = audioInputStream.read(bytesBuffer)) != -1) {
                line.write(bytesBuffer, 0, bytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        line.drain();
        line.close();
        try {
            audioInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
